################################################################################
# General
################################################################################
export EDITOR='vim'
export VISUAL='vim'
export KEYTIMEOUT=1        # Eliminates <Esc> delay for Vi mode.
export TERM=screen-256color

# Prezto
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Rust
if [[ -s "${CARGO_HOME:-$HOME/.cargo}/env" ]]; then
  source "${CARGO_HOME:-$HOME/.cargo}/env"
fi

# Dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


################################################################################
# Vi Mode
################################################################################  
bindkey -v

# Re-mappings - qwerty -> colemak:
# n -> j (down)
# e -> k (up)
# i -> l (right)
# j -> e (end of word)
# k -> n (next in search)
# l -> i (insert mode)
# L -> I (insert at bol)
bindkey -M vicmd "n" down-line-or-history
bindkey -M vicmd "e" up-line-or-history
bindkey -M vicmd "i" forward-char
bindkey -M vicmd "j" vi-forward-word-end
bindkey -M vicmd "k" vi-repeat-search
bindkey -M vicmd "l" vi-insert
bindkey -M vicmd "L" vi-insert-bol

# Sane Undo, Redo, Backspace, Delete
bindkey -M vicmd "u" undo
bindkey -M vicmd "U" redo
bindkey -M vicmd "^?" backward-delete-char
bindkey -M vicmd "^[[3~" delete-char

# Keep ctrl+r searching
bindkey -M viins '^R' history-incremental-pattern-search-forward
bindkey -M viins '^r' history-incremental-pattern-search-backward
