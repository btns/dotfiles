""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colemak-Vim Mappings
"
" Re-mappings - qwerty -> colemak:
" n -> j (down)
" e -> k (up)
" i -> l (right)
" j -> e (end of word)
" k -> n (next in search)
" l -> i (insert mode)
" I -> L (screen bottom)
" L -> I (insert at bol)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap n j
noremap e k
noremap i l
noremap j e
noremap k n
noremap l i
noremap I L
noremap L I
